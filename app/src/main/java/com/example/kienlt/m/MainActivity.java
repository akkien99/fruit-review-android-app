package com.example.kienlt.m;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ScrollView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lViewFruit;
    ArrayList<Fruit> fruitArray;
    FruitAdapter fruitAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initial();

        fruitAdapter = new FruitAdapter(
                MainActivity.this,
                R.layout.fruit_item,
                fruitArray
        );

        lViewFruit.setAdapter(fruitAdapter);
    }

    private void initial(){
        lViewFruit = findViewById(R.id.lViewFruit);
        fruitArray = new ArrayList<>();

        fruitArray.add(new Fruit(R.drawable.banana, "Chuoi", "Chuoi la qua mau vang. buong nhieu nay, nay nhieu trai..."));
        fruitArray.add(new Fruit(R.drawable.grape, "Nho", "Nho mau tim, dung lam ruou vang..."));
        fruitArray.add(new Fruit(R.drawable.watermelon, "Dua hau", "Dua hau do Mai An Tim tim ra, ngoai xanh, trong do, hot den..."));
        fruitArray.add(new Fruit(R.drawable.mango, "Xoai", "Xoai just xoai..."));
        fruitArray.add(new Fruit(R.drawable.strawberry, "Dau tay", "La dau o ben Tay do ma..."));
    }
}
