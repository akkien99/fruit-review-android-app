package com.example.kienlt.m;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class FruitAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Fruit> fruitList;

    public FruitAdapter(Context context, int layout, List<Fruit> fruitList){
        this.context = context;
        this.layout = layout;
        this.fruitList = fruitList;
    }

    @Override
    public int getCount() {
        return fruitList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private class ViewHolder{
        ImageView imgAvatar;
        TextView txtName;
        TextView txtDesc;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout, null);

            holder = new ViewHolder();
            holder.imgAvatar = view.findViewById(R.id.avatar);
            holder.txtName = view.findViewById(R.id.name);
            holder.txtDesc = view.findViewById(R.id.desc);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }



        Fruit fruitItem = fruitList.get(i);

        holder.imgAvatar.setImageResource(fruitItem.getAvatar());
        holder.txtName.setText(fruitItem.getName());
        holder.txtDesc.setText(fruitItem.getDesc());

        return view;
    }
}
